<?php

/**
 * 最新記事リスト
 * 
 * @param int|string cat: カテゴリー指定。ない場合は全カテゴリから取得。
 * @param int   num: 取得件数。
 * 
 * @return string
 */
function get_new_items($atts) {
    shortcode_atts(array(
        "num" => 3, //取得数
        "cat" => '' //カテゴリーID指定
    ), $atts);
    global $post;
    $oldpost = $post;
    $myposts = get_posts([
        'category' => (string)$atts['cat'],
        'posts_per_page' => (int)$atts['num'],
    ]);

    ob_start();
    echo '<div class="postList">';
    foreach($myposts as $post) {
        get_template_part( 'template-parts/post/loop');
    }
    echo '</div>';
    $retHtml = ob_get_clean();

    $post = $oldpost;
    wp_reset_postdata();

    return $retHtml;
}
